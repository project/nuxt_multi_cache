<?php

namespace Drupal\graphql_nuxt_multi_cache\Plugin\GraphQL\Fields;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\nuxt_multi_cache\CacheTagHelper;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @GraphQLField(
 *   id = "cache_tags",
 *   secure = true,
 *   name = "cacheTags",
 *   type = "[String]",
 *   parents = {"Entity"}
 * )
 */
class CacheTags extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The cache tag helper.
   *
   * @var \Drupal\nuxt_multi_cache\CacheTagHelper
   */
  protected $cacheTagHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('nuxt_multi_cache.cache_tag_helper')
    );
  }

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\nuxt_multi_cache\CacheTagHelper
   *   The cache tag helper.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    CacheTagHelper $cacheTagHelper
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->cacheTagHelper = $cacheTagHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface) {
      $tags = $value->getCacheTagsToInvalidate();
      $filtered = $this->cacheTagHelper->filterBannedTags($tags);
      $encoded = $this->cacheTagHelper->encodeTags($filtered);

      foreach ($encoded as $tag) {
        yield $tag;
      }
    }
  }

}
