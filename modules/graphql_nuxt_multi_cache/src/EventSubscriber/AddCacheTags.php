<?php

namespace Drupal\graphql_nuxt_multi_cache\EventSubscriber;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\nuxt_multi_cache\CacheTagHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Adds cache tag headers for the nuxt-multi-cache module.
 */
class AddCacheTags implements EventSubscriberInterface {

  /**
   * The cache tag helper.
   *
   * @var \Drupal\nuxt_multi_cache\CacheTagHelper
   */
  protected $cacheTagHelper;

  /**
   * Constructs a new CacheTagsHeaderLimitDetector object.
   *
   * @param \Drupal\nuxt_multi_cache\CacheTagHelper
   *   The cache tag helper.
   */
  public function __construct(CacheTagHelper $cache_tag_helper) {
    $this->cacheTagHelper = $cache_tag_helper;
  }

  /**
   * Adds Surrogate-Control header.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond($event) {
    // Only modify the master request.
    if ((!$event->isMainRequest())) {
      return;
    }

    // Get response.
    // Drupal\Core\Cache\CacheableJsonResponse
    $response = $event->getResponse();
    if ($response instanceof CacheableJsonResponse) {
      $cache_tags = $response->getCacheableMetadata()->getCacheTags();
      $filtered = $this->cacheTagHelper->filterBannedTags($cache_tags);
      $encoded = $this->cacheTagHelper->encodeTags($filtered);

      // Set the encoded and filtered cache tag header.
      $response->headers->set('X-Nuxt-Cache-Tags', implode(' ', $encoded));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}