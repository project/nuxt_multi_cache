<?php

namespace Drupal\nuxt_multi_cache_purger\Plugin\Purge\Purger;

use Drupal\nuxt_multi_cache\CacheTagHelper;
use Drupal\nuxt_multi_cache\NuxtMultiCacheApi;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Nuxt Multi Cache Purger.
 *
 * @PurgePurger(
 *   id = "nuxt_multi_cache",
 *   label = @Translation("Nuxt Multi Cache"),
 *   description = @Translation("Purger for Nuxt Multi Cache."),
 *   types = {"tag", "url", "everything"},
 *   multi_instance = FALSE,
 * )
 */
class NuxtMultiCachePurger extends PurgerBase implements PurgerInterface {

  /**
   * The API service.
   *
   * @var \Drupal\nuxt_multi_cache\NuxtMultiCacheApi
   */
  protected $api;

  /**
   * The cache tag helper.
   *
   * @var \Drupal\nuxt_multi_cache\CacheTagHelper
   */
  protected $cacheTagHelper;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('nuxt_multi_cache.api'),
      $container->get('nuxt_multi_cache.cache_tag_helper'),
      $container->get('logger.channel.nuxt_multi_cache'),
    );
  }

  /**
   * Constructs a NuxtMultiCachePurger instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\nuxt_multi_cache\NuxtMultiCacheApi $api
   *   The API service.
   * @param \Drupal\nuxt_multi_cache\CacheTagHelper
   *   The cache tag helper.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @throws \LogicException
   *   Thrown if $configuration['id'] is missing, see Purger\Service::createId.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, NuxtMultiCacheApi $api, CacheTagHelper $cache_tag_helper, LoggerChannelInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->api = $api;
    $this->cacheTagHelper = $cache_tag_helper;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'tag'  => 'invalidateTags',
      'url'  => 'invalidateUrls',
      'everything' => 'invalidateAll',
    ];
    return isset($methods[$type]) ? $methods[$type] : 'invalidate';
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    throw new \LogicException('This should not execute.');
  }

  /**
   * Invalidate a set of urls.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateUrls(array $invalidations) {
    $urls = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /* @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $urls[] = $invalidation->getExpression();
    }

    if (empty($urls)) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
        throw new \Exception('No url found to purge');
      }
    }

    $invalidation_state = $this->invalidateItems('urls', $urls);
    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate a set of tags.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateTags(array $invalidations) {
    $tags = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /* @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $tags[] = $invalidation->getExpression();
    }

    if (empty($tags)) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
        throw new \Exception('No tag found to purge');
      }
    }

    $invalidation_state = $this->invalidateItems('tags', $tags);
    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate everything.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   */
  public function invalidateAll(array $invalidations) {
    $this->updateState($invalidations, InvalidationInterface::PROCESSING);
    // Invalidate and update the item state.
    $invalidation_state = $this->invalidateItems();
    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate cache.
   *
   * @param mixed $type
   *   Type to purge like tags/url. If null, will purge everything.
   * @param string[] $invalidates
   *   A list of items to invalidate.
   *
   * @return int
   *   Returns invalidate items.
   */
  protected function invalidateItems($type = NULL, array $invalidates = []) {
    try {
      if ($type === 'tags') {
        $tags = $this->cacheTagHelper->encodeTags($invalidates);
        $purged = $this->api->purgeTags($tags, TRUE);
      }
      elseif ($type === 'urls') {
        $purged = $this->api->purgePages($invalidates);
      }
      else {
        $purged = $this->api->purgeAll();
      }
      if ($purged !== NULL) {
        return InvalidationInterface::SUCCEEDED;
      }
      return InvalidationInterface::FAILED;
    }
    catch (\Exception $e) {
      return InvalidationInterface::FAILED;
    }
  }

  /**
   * Update the invalidation state of items.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   * @param int $invalidation_state
   *   The invalidation state.
   */
  protected function updateState(array $invalidations, $invalidation_state) {
    // Update the state.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

}
