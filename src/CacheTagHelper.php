<?php

declare(strict_types = 1);

namespace Drupal\nuxt_multi_cache;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Cache tags helper.
 */
class CacheTagHelper {

  /**
   * Array of cache tag prefix to its short version.
   *
   * @param array
   */
  protected $prefixMap;

  /**
   * Array of cache tag short version to its long version.
   *
   * @param array
   */
  protected $prefixMapReverse;

  /**
   * Array of cache tag to its short version.
   *
   * @param array
   */
  protected $tagMap;

  /**
   * Array of cache tag short version to its long version.
   *
   * @param array
   */
  protected $tagMapReverse;

  /**
   * Array of banned tags.
   *
   * @param array
   */
  protected $banned;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * NuxtMultiCacheApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('nuxt_multi_cache.settings');
    $this->prefixMap = [
      'node' => 'N',
      'taxonomy_term' => 'T',
      'block_content' => 'B',
      'file' => 'F',
      'media' => 'M',
      'user' => 'U',
      'menu_link_content' => 'L',
    ];
    $this->prefixMapReverse = array_flip($this->prefixMap);
    $this->tagMap = [];
    $this->tagMapReverse = [];

    $mapping = $this->config->get('mapping') ?? [];

    foreach ($mapping as $map) {
      $parts = explode(';', $map);
      $this->tagMap[$parts[0]] = $parts[1];
    }
    $this->tagMapReverse = array_flip($this->tagMap);
    $this->banned = $this->config->get('ban_tags') ?? [];
  }

  /**
   * Filters an array of tags to remove banned tags.
   *
   * @param string[] $tags
   *   Tags to filter.
   *
   * @return string[]
   *   The filtered tags.
   */
  public function filterBannedTags(array $tags): array {
    return array_filter($tags, function ($tag) {
      return !in_array($tag, $this->banned);
    });
  }

  /**
   * Encode tags to their short form.
   */
  public function encodeTags(array $tags): array {
    $encoded = [];

    foreach ($tags as $tag) {
      $encoded[] = $this->encodeTag($tag);
    }

    return $encoded;
  }

  /**
   * Encode a tag.
   *
   * @param string $tag
   *   The tag to encode.
   *
   * @return string
   *   The encoded tag.
   */
  public function encodeTag(string $tag): string {
    if (!empty($this->tagMap[$tag])) {
      return $this->tagMap[$tag];
    }

    $parts = explode(':', $tag, 2);
    if (empty($parts[1])) {
      return $tag;
    }

    $first = $parts[0];
    $short = $this->prefixMap[$first] ?? $first;
    return "$short:$parts[1]";
  }

  /**
   * Decode a tag.
   *
   * @param string $tag
   *   The tag to decode.
   *
   * @return string
   *   The decoded tag.
   */
  public function decodeTag(string $tag): string {
    if (!empty($this->tagMapReverse[$tag])) {
      return $this->tagMapReverse[$tag];
    }

    $parts = explode(':', $tag, 2);
    if (empty($parts[1])) {
      return $tag;
    }

    $first = $parts[0];
    $short = $this->prefixMapReverse[$first] ?? $first;
    return "$short:$parts[1]";

  }

}
