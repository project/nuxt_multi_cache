<?php

declare(strict_types = 1);

namespace Drupal\nuxt_multi_cache;

use GuzzleHttp\Exception\ClientException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Nuxt Multi Cache API Service.
 */
class NuxtMultiCacheApi {

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * NuxtMultiCacheApi constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   * @param \GuzzleHttp\ClientInterface $client
   *   HTTP Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    LoggerChannelInterface $logger,
    ClientInterface $client,
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger
  ) {
    $this->logger = $logger;
    $this->client = $client;
    $this->config = $config_factory->get('nuxt_multi_cache.settings');
    $this->messenger = $messenger;
  }

  /**
   * Make a request to the Nuxt Multi Cache API.
   *
   * @param string $path
   *   The resource path.
   * @param array $parameters
   *   Query parameters.
   * @param string $method
   *   Query method, defaults to GET.
   *
   * @return array
   *   The response data.
   */
  public function request($path, array $parameters = [], $method = 'get'): ?array {
    $url = $this->config->get('endpoint');
    $username = $this->config->get('username');
    $password = $this->config->get('password');

    $parameters = array_merge($parameters, [
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode("$username:$password"),
      ],
    ]);

    try {
      if ($method === 'get') {
        $response = $this->client->get($url . $path, $parameters);
      }
      else {
        $response = $this->client->post($url . $path, $parameters);
      }

      if ($response !== NULL) {
        return json_decode((string) $response->getBody(), TRUE);
      }
    }
    catch (ClientException $exception) {
      $code = $exception->getCode();
      $this->logger->error($exception->getMessage());
      if ($code === 403) {
        $this->messenger->addError('Invalid credentials provided for nuxt-multi-cache endpoint.');
      }
    }

    return [];
  }

  public function getPages($offset = 0, $tag = ''): array {
    $result = $this->request('/stats/page', [
      'query' => [
        'offset' => $offset,
        'tag' => $tag,
      ],
    ]);

    return [
      'total' => $result['total'] ?? 0,
      'rows' => $result['rows'] ?? [],
    ];
  }

  public function getTags($offset = 0): array {
    $result = $this->request('/stats/tags', [
      'query' => [
        'offset' => $offset,
      ],
    ]);

    return [
      'total' => $result['total'] ?? 0,
      'rows' => $result['rows'] ?? [],
    ];
  }

  public function getComponents($offset = 0): array {
    $result = $this->request('/stats/component', [
      'query' => [
        'offset' => $offset,
      ],
    ]);

    return [
      'total' => $result['total'] ?? 0,
      'rows' => $result['rows'] ?? [],
    ];
  }

  public function getData($offset = 0): array {
    $result = $this->request('/stats/data', [
      'query' => [
        'offset' => $offset,
      ],
    ]);

    return [
      'total' => $result['total'] ?? 0,
      'rows' => $result['rows'] ?? [],
    ];
  }

  public function getCacheGroups(): array {
    $result = $this->request('/stats/groups');

    return [
      'total' => $result['total'] ?? 0,
      'rows' => $result['rows'] ?? [],
    ];
  }

  public function purgePages(array $keys): ?array {
    return $this->request('/purge/page', ['json' => $keys], 'POST');
  }

  public function purgeTags(array $tags, $log = FALSE): ?array {
    $result = $this->request('/purge/tags', ['json' => $tags], 'POST');

    if ($log) {
      $this->logResult($result['tags'], $result['routes'], $result['components'], $result['data']);
    }

    return $result;
  }

  public function purgeComponents(array $keys): ?array {
    return $this->request('/purge/component', ['json' => $keys], 'POST');
  }

  public function purgeData(array $keys): ?array {
    return $this->request('/purge/data', ['json' => $keys], 'POST');
  }

  public function purgeAll(): ?array {
    return $this->request('/purge/all', [], 'POST');
  }

  protected function logResult($tags, $routes, $components, $data) {
    $messages = [
      ['Purged tags', implode('<br>', $tags)]
    ];

    if ($routes) {
      $count = $routes['purged'];
      $messages[] = ['Purged routes', $count];
    }

    if ($components) {
      $count = $components['purged'];
      $messages[] = ['Purged components', $count];
    }

    if ($data) {
      $count = $data['purged'];
      $messages[] = ['Purged data entries', $count];
    }

    $rows = implode("\n", array_map(function($item) {
      return "<tr><th>$item[0]</th><td>$item[1]</td></tr>";
    }, $messages));

    $this->logger->info("<table>$rows</table>");
  }

}
