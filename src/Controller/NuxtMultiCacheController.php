<?php

namespace Drupal\nuxt_multi_cache\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\nuxt_multi_cache\NuxtMultiCacheApi;
use Symfony\Component\HttpFoundation\Request;

/**
 * Define routes for managing nuxt-multi-cache actions.
 */
class NuxtMultiCacheController extends ControllerBase {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The API service.
   *
   * @var \Drupal\nuxt_multi_cache\NuxtMultiCacheApi
   */
  protected $api;

  /**
   * Constructs a new ListRoutes form.
   *
   * @param \Drupal\nuxt_multi_cache\NuxtMultiCacheApi $api
   *   The API service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(NuxtMultiCacheApi $api, Request $request) {
    $this->api = $api;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nuxt_multi_cache.api'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * Purge everything.
   */
  public function purgeAll() {
    $result = $this->api->purgeAll();
    if (!empty($result['success'])) {
      $this->messenger()->addStatus($this->t('Purged all caches.', []), 'status');
    }
    return $this->redirect('nuxt_multi_cache.settings');
  }

  /**
   * Purge a single route.
   */
  public function purgePage() {
    $key = $this->request->query->get('key');
    if ($key) {
      $result = $this->api->purgePages([$key]);
      if (!empty($result['success'])) {
        $this->messenger()->addStatus($this->t('Purged page: @key.', [
          '@key' => $key,
        ]), 'status');
      }
    }
    return $this->redirect('nuxt_multi_cache.pages');
  }

  /**
   * Purge a single tag.
   */
  public function purgeTag($tag = '') {
    $this->api->purgeTags([$tag], TRUE);
    $this->messenger()->addStatus($this->t('Purged tag "@tag".', [
      '@tag' => $tag,
    ]), 'status');
    return $this->redirect('nuxt_multi_cache.tags');
  }

  /**
   * Purge a single component.
   */
  public function purgeComponent() {
    $component = $this->request->query->get('component');
    if ($component) {
      $result = $this->api->purgeComponents([$component]);
      if (!empty($result['success'])) {
        $this->messenger()->addStatus($this->t('Purged component: @component.', [
          '@component' => $component,
        ]), 'status');
      }
    }
    return $this->redirect('nuxt_multi_cache.components');
  }

  /**
   * Purge a single component.
   */
  public function purgeData() {
    $key = $this->request->query->get('key');
    if ($key) {
      $result = $this->api->purgeData([$key]);
      if (!empty($result['success'])) {
        $this->messenger()->addStatus($this->t('Purged data cache entry: @key.', [
          '@key' => $key,
        ]), 'status');
      }
    }
    return $this->redirect('nuxt_multi_cache.data');
  }

}
