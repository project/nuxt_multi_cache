<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ListTagsDetail extends ListTags {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_tags_detail';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $value = \Drupal::request()->query->get('tags') ?? '';
    if (!$value) {
      return [];
    }

    $tags = explode(',', $value);
    $rows = array_map(function ($tag) {
      $decoded = $this->cacheTagHelper->decodeTag($tag);
      $link = $this->getLinkForCacheTag($decoded);
      $operations = $this->getOperations($tag);

      return [
        $tag,
        $decoded,
        $link,
        $operations
      ];
    }, $tags);

    $form['tags'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Tag'),
        $this->t('Tag') . ' (' . $this->t('Decoded') . ') ',
        $this->t('Entity'),
        $this->t('Actions'),
      ],
      '#rows' => $rows,
    ];
    return $form;
  }
}
