<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ListCacheGroups extends ListBase {

  public const PER_PAGE = 48;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_cache_groups';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#markup' => $this->t('Cache groups are collections for tags that belong to a single thing, like a query or a component. The typical use case is for components that appear on every page (like a menu). Instead of adding these cache tags for every cache entry, a cache group can be added instead.'),
    ];
    $data = $this->api->getCacheGroups();

    $rows = array_map(function ($row) {
      $name = $row['name'];
      $tags = $row['tags'];
      $tags_link = $this->getTagsLink($tags);

      return [$name, $tags_link];
    }, $data['rows']);

    $form['routes'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Tags'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('No cache groups.'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
