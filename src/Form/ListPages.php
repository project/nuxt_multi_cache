<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\nuxt_multi_cache\NuxtMultiCacheApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class ListPages extends ListBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_pages';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage();
    $offset = self::PER_PAGE * $page;
    $frontend_url = $this->config->get('frontend');

    $routes = $this->getPages($offset);
    $pager_manager = \Drupal::service('pager.manager');
    $pager_manager->createPager($routes['total'], self::PER_PAGE);

    $rows = array_map(function ($row) use ($frontend_url) {
      $tags = $row['tags'];
      $key = $row['key'];
      $timestamp = $row['timestamp'];
      $date_formatted = \Drupal::service('date.formatter')->format($timestamp / 1000, 'short');
      $frontend_url = Url::fromUri($frontend_url . $key);
      $tags_link = $this->getTagsLink($tags);
      $route = [
        'data' => [
          '#type' => 'link',
          '#title' => $key,
          '#url' => $frontend_url,
          '#attributes' => [
            'target' => '_blank',
          ],
        ],
      ];

      $purge_url = Url::fromRoute('nuxt_multi_cache.purge_page');
      $purge_url->setOption('query', ['key' => $key]);
      $operations = [
        'data' => [
          '#type' => 'dropbutton',
          '#links' => [
            'view' => [
              'title' => $this->t('Purge'),
              'url' => $purge_url,
            ],
          ],
        ],
      ];

      return [$route, $tags_link, $date_formatted, $operations];
    }, $routes['rows']);

    $form['routes'] = [
      '#theme' => 'table',
      '#header' => ['Key', 'Tags', 'Date', 'Operations'],
      '#rows' => $rows,
      '#empty' => $this->t('No cached pages'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  protected function getPages($offset) {
    return $this->api->getPages($offset);
  }

}
