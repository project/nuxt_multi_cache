<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\nuxt_multi_cache\CacheTagHelper;
use Drupal\nuxt_multi_cache\NuxtMultiCacheApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class ListTags extends FormBase {

  public const PER_PAGE = 196;

  /**
   * The API service.
   *
   * @var \Drupal\nuxt_multi_cache\NuxtMultiCacheApi
   */
  protected $api;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache tag helper.
   *
   * @var \Drupal\nuxt_multi_cache\CacheTagHelper
   */
  protected $cacheTagHelper;

  /**
   * Constructs a new ListRoutes form.
   *
   * @param \Drupal\nuxt_multi_cache\NuxtMultiCacheApi $api
   *   The API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\nuxt_multi_cache\CacheTagHelper
   *   The cache tag helper.
   */
  public function __construct(NuxtMultiCacheApi $api, EntityTypeManagerInterface $entity_type_manager, CacheTagHelper $cache_tag_helper) {
    $this->api = $api;
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheTagHelper = $cache_tag_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nuxt_multi_cache.api'),
      $container->get('entity_type.manager'),
      $container->get('nuxt_multi_cache.cache_tag_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_tags';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage();
    $offset = self::PER_PAGE * $page;

    $data = $this->api->getTags($offset);
    $pager_manager = \Drupal::service('pager.manager');
    $pager_manager->createPager($data['total'], self::PER_PAGE);

    $rows = array_map(function ($row) {
      $tag = $row['tag'];
      $decoded = $this->cacheTagHelper->decodeTag($tag);
      $total = $row['total'];
      $count_page = $row['counts']['page'] ?? '';
      $count_component = $row['counts']['component'] ?? '';
      $count_data = $row['counts']['data'] ?? '';
      $count_groups = $row['counts']['data'] ?? '';
      $link = $this->getLinkForCacheTag($tag);
      $operations = $this->getOperations($tag);

      return [
        $tag,
        $decoded,
        $link,
        $total,
        $count_page,
        $count_component,
        $count_data,
        $count_groups,
        $operations,
      ];
    }, $data['rows']);

    $form['routes'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Tag'),
        $this->t('Tag') . ' (' . $this->t('Decoded') . ') ',
        $this->t('Entity'),
        $this->t('Usage'),
        $this->t('Pages'),
        $this->t('Components'),
        $this->t('Data'),
        $this->t('Groups'),
        $this->t('Actions'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('No cache tags.'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  protected function getOperations(string $tag): array {
    return [
      'data' => [
        '#type' => 'dropbutton',
        '#links' => [
          'view' => [
            'title' => $this->t('Purge'),
            'url' => Url::fromRoute('nuxt_multi_cache.tag_purge', ['tag' => $tag]),
          ],
        ],
      ],
    ];
  }

  protected function getLinkForCacheTag($tag = ''): ?array {
    $decoded = $this->cacheTagHelper->decodeTag($tag);
    $parts = explode(':', $decoded);

    if (count($parts) !== 2) {
      return NULL;
    }
    if (!$this->entityTypeManager->hasDefinition($parts[0])) {
      return NULL;
    }

    $storage = $this->entityTypeManager->getStorage($parts[0]);
    $entity = $storage->load($parts[1]);

    if (!$entity) {
      return NULL;
    }

    return [
      'data' => [
        '#type' => 'link',
        '#title' => mb_strimwidth($entity->label(), 0, 64, '...'),
        '#url' => $this->getEntityUrl($entity),
      ],
    ];
  }

  private function getEntityUrl(EntityInterface $entity): ?Url {
    if ($entity instanceof FileInterface) {
      return Url::fromUri($entity->createFileUrl(FALSE));
    }
    return $entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
