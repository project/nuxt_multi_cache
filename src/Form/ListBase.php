<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\nuxt_multi_cache\NuxtMultiCacheApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class ListBase extends FormBase {

  public const PER_PAGE = 100;

  /**
   * The API service.
   *
   * @var \Drupal\nuxt_multi_cache\NuxtMultiCacheApi
   */
  protected $api;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new ListPages form.
   *
   * @param \Drupal\nuxt_multi_cache\NuxtMultiCacheApi $api
   *   The API service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(
    NuxtMultiCacheApi $api,
    RouteMatchInterface $route_match,
    ConfigFactoryInterface $config_factory
  ) {
    $this->api = $api;
    $this->routeMatch = $route_match;
    $this->config = $config_factory->get('nuxt_multi_cache.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nuxt_multi_cache.api'),
      $container->get('current_route_match'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_routes';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  public function getTagsLink($tags) {
    $count = count($tags);
    if (!$count) {
      return [];
    }
    $title = $this->formatPlural($count, '@count tag', '@count tags');
    return [
      'data' => [
        '#type' => 'link',
        '#title' => $title,
        '#url' => Url::fromRoute('nuxt_multi_cache.tags_detail', [
          'tags' => implode(',', $tags),
        ]),
        '#attributes' => [
          'class' => 'use-ajax',
          'data-dialog-type' => 'modal',
          'data-dialog-options' => '{ "width": 1280 }',
        ],
      ]
    ];
  }

}
