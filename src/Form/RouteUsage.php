<?php

namespace Drupal\nuxt_multi_cache\Form;

/**
 * Configure example settings for this site.
 */
class RouteUsage extends ListPages {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_usage_list_routes';
  }

  protected function getRoutes($offset) {
    $node_id = $this->routeMatch->getParameter('node');
    $tag = 'node:' . $node_id;
    return $this->api->getPages($offset, $tag);
  }

}
