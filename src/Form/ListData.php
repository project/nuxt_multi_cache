<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure example settings for this site.
 */
class ListData extends ListBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_list_data';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pager_parameters = \Drupal::service('pager.parameters');
    $page = $pager_parameters->findPage();
    $offset = self::PER_PAGE * $page;

    $data = $this->api->getData($offset);
    $pager_manager = \Drupal::service('pager.manager');
    $pager_manager->createPager($data['total'], self::PER_PAGE);

    $rows = array_map(function ($row) {
      $key = $row['key'];
      $tags = $row['tags'];
      $timestamp = $row['timestamp'];
      $tags_link = $this->getTagsLink($tags);

      $date_formatted = \Drupal::service('date.formatter')->format($timestamp / 1000, 'short');

      $operations = [
        'data' => [
          '#type' => 'dropbutton',
          '#links' => [
            'view' => [
              'title' => $this->t('Purge'),
              'url' => Url::fromRoute('nuxt_multi_cache.purge_data', ['key' => $key]),
            ],
          ],
        ],
      ];

      return [$key, $tags_link, $date_formatted, $operations];
    }, $data['rows']);

    $form['routes'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Key'),
        $this->t('Tags'),
        $this->t('Timestamp'),
        $this->t('Actions'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('No cached data.'),
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

}
