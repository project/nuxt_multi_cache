<?php

namespace Drupal\nuxt_multi_cache\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxt_multi_cache_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config('nuxt_multi_cache.settings');

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#default_value' => $config->get('endpoint'),
      '#description' => $this->t('URL of the nuxt-multi-cache endpoint.'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#default_value' => $config->get('username'),
      '#description' => $this->t('User name for basic auth.'),
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('password'),
      '#description' => $this->t('Password for basic auth.'),
    ];

    $form['frontend'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Frontend'),
      '#default_value' => $config->get('frontend'),
      '#description' => $this->t('URL of the frontend application. Used to generate route direct links.'),
    ];

    $ban_tags = $config->get('ban_tags') ?? [];
    $ban_tags = implode("\r\n", $ban_tags);
    $form['ban_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Banned tags'),
      '#default_value' => $ban_tags,
      '#description' => $this->t('Enter cache tags (one per line) that should be filtered. These are not sent back in the cache tag header.'),
    ];

    $mapping = $config->get('mapping') ?? [];
    $mapping = implode("\r\n", $mapping);
    $form['mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tag mapping'),
      '#default_value' => $mapping,
      '#description' => $this->t('Map common cache tags to reduce the HTTP header size. One mapping per line using the following syntax: tag_name.property.key;TPK'),
    ];

    $form['actions']['purge_all'] = [
      '#type' => 'link',
      '#title' => $this->t('Purge everything'),
      '#url' => Url::fromRoute('nuxt_multi_cache.purge_all'),
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nuxt_multi_cache.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nuxt_multi_cache.settings');
    $config->set('endpoint', $form_state->getValue('endpoint'));
    $config->set('username', $form_state->getValue('username'));
    $config->set('password', $form_state->getValue('password'));
    $config->set('frontend', $form_state->getValue('frontend'));

    $mapping = explode("\r\n", $form_state->getValue('mapping'));
    $config->set('mapping', $mapping);

    $ban_tags = explode("\r\n", $form_state->getValue('ban_tags'));
    $config->set('ban_tags', $ban_tags);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
